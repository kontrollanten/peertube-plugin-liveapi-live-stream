const createRoutes = require('./routes');
const createLiveApiClient = require('./liveapi-client');
const onLiveVideoCreated = require('./on-live-video-created');
const onLiveVideoDeleted = require('./on-live-video-deleted');
const { ROUTES } = require('./constants');

async function register ({
  getRouter,
  registerHook,
  registerSetting,
  peertubeHelpers,
  settingsManager,
  storageManager
}) {
  const { database, logger, socket, videos } = peertubeHelpers;
  const serverUrl = peertubeHelpers.config.getWebserverUrl();

  const getLiveApiClient = async () => {
    const { accessTokenId, secretKey } = await settingsManager.getSettings(['accessTokenId', 'secretKey']);

    return createLiveApiClient({
      accessTokenId,
      logger,
      secretKey,
    });
  };

  registerSetting({
    name: 'accessTokenId',
    label: 'LiveAPI access token id',
    type: 'input',
    private: true,
  });

  registerSetting({
    name: 'secretKey',
    label: 'LiveAPI secret key',
    type: 'input',
    private: true,
  });

  registerSetting({
    name: 'forgivenessTimeout',
    label: 'Forgiveness timeout',
    type: 'input',
    default: '5',
    private: true,
    descriptionHTML:
      'Time in minutes before a stream-disconnected event will be treated as live has ended. ' +
      'Longer period means more time to reconnect a crashed stream, but also longer period until replay gets published.'
  });

  registerSetting({
    name: 'webhookUrl',
    label: 'LiveAPI Webhook URL',
    type: 'html',
    html: `${serverUrl}/plugins/liveapi-live-stream/router${ROUTES.LIVE_API_WEBHOOK}`
  });

  registerHook({
    target: 'action:api.live-video.created',
    handler: async ({ video }) => {
      /**
       * TODO: Run in await and return user error upon failure
       */
      try {
        onLiveVideoCreated({
          getLiveApiClient,
          logger,
          settingsManager,
          storageManager,
          video,
          webserverHostname: serverUrl.split('//')[1],
        });
      } catch (error) {
        logger.error(error);
      }
    },
  });

  registerHook({
    target: 'action:api.video.deleted',
    handler: async ({ video }) => {
      if (!video.isLive) return

      try {
        await onLiveVideoDeleted({
          getLiveApiClient,
          logger,
          storageManager,
          video,
        });
      } catch (error) {
        logger.error(error);
      }
    },
  });

  createRoutes({
    database,
    getLiveApiClient,
    logger,
    router: getRouter(),
    settingsManager,
    storageManager,
    socket,
    videos,
  });
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}

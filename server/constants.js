module.exports = {
  ROUTES: {
    LIVE_API_WEBHOOK: '/live-api-webhook',
  },
  STORE_KEYS: {
    LIVE_API_PT_ID_MAPPING_PREFIX: 'live-stream-api-',
    STREAM_DATA_PREFIX: 'stream-',
    WEBHOOKS: 'webhooks',
  },
};

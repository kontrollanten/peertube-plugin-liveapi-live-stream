const { ROUTES } = require('../constants');
const createWebhookRoute = require('./webhook');
const createLiveStreamRoute = require('./live-stream');

module.exports = ({
  database,
  getLiveApiClient,
  logger,
  router,
  socket,
  settingsManager,
  storageManager,
  videos,
}) => {
  router.post(ROUTES.LIVE_API_WEBHOOK, createWebhookRoute({
    database,
    getLiveApiClient,
    settingsManager,
    storageManager,
    logger,
    socket,
    videos
  }));

  router.get('/live-stream/:videoUUID', createLiveStreamRoute({
    database,
    storageManager,
    logger
  }));
};


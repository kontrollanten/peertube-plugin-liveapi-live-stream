async function register ({
  peertubeHelpers,
  registerHook,
}) {
  const { getAuthHeader } = peertubeHelpers;

  const getLiveStream = (uuid) =>
    fetch(`/plugins/liveapi-live-stream/router/live-stream/${uuid}`, {
      headers: getAuthHeader()
    })
      .then(res => res.ok && res.json());

  registerHook({
    target: 'filter:internal.video-watch.player.build-options.result',
    handler: (options) => {
      console.log('filter:internal.video-watch.player.build-options.result', { options })

      if (options.playerOptions.common.isLive) {
        options.playerOptions.common.useP2P = false;
        options.playerOptions.p2pMediaLoader.videoFiles = [];
      }

      return options
    }
  })

  registerHook({
    target: 'filter:internal.video-watch.player.build-options.params',
    handler: async (params) => {
      console.log('filter:internal.video-watch.player.build-options.params', { params })

      if (params.video.isLive && params.video.streamingPlaylists) {
        const streamData = await getLiveStream(params.video.uuid);

        params.video.streamingPlaylists = [
          {
            type: 1,
            playlistUrl: streamData.playlistUrl,
            redundancies: []
          }
        ];
      }

      return params;
    },
  });

  registerHook({
    target: 'action:video-edit.init',
    handler: async ({ type }) => {
      console.log('video edit init', type);
      if (type !== 'update') return;

      const videoUUID = location.pathname.split('/').pop();
      /**
       * TODO:
       * - Poll until result is given, show progress
       * - Don't run request if it's not a live video
       * - Don't run request if the live has ended
       */
      const result = await getLiveStream(videoUUID);

      if (!result) return

      peertubeHelpers.showModal({
        title: 'Stream settings',
        content: `
        <div>
          <label>RTMPS URL</label>
          <div style="user-select: all;">
            ${result.rtmps.url}
          </div>
        </div>
        <div>
          <label>Stream key</label>
          <div style="user-select: all;">
            ${result.rtmps.streamKey}
          </div>
        </div>
        `,
        close: true,
      });
    },
  });
}

export {
  register
}

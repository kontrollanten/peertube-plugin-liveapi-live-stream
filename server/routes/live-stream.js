const { STORE_KEYS } = require('../constants');

module.exports = ({
  database,
  storageManager,
  logger
}) => async (req, res) => {
  const videoUUID = req.params.videoUUID;
  const streamData = await storageManager.getData(STORE_KEYS.STREAM_DATA_PREFIX + videoUUID);

  if (!streamData) return res.status(404).end();

  const {
    uid,
    rtmps,
    playlistUrl
  } = streamData;
  let output = {
    playlistUrl
  };

  if (res.locals.oauth) {
    try {
      const [{ id }] = await database.query(`
        SELECT video.id FROM video
        INNER JOIN "videoChannel" ON "videoChannel".id = video."channelId"
        WHERE video.uuid = ? AND "videoChannel"."accountId" = ?
      `, {
        type: 'SELECT',
        replacements: [videoUUID, res.locals.oauth.token.User.Account.id],
        raw: true,
        logging: logger.debug.bind(logger)
      });

      if (id) {
        output = {
          ...output,
          uid,
          rtmps
        }
      }
    } catch (error) {} // eslint-disable-line
  }


  res.json(output);
}
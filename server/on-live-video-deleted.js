const { STORE_KEYS } = require('./constants');

module.exports = async ({
  getLiveApiClient,
  logger,
  storageManager,
  video,
}) => {
  logger.debug(`Live video ${video.id} deleted, running cleanup.`);

  const storeKey = STORE_KEYS.STREAM_DATA_PREFIX + video.uuid;
  const streamData = await storageManager.getData(storeKey);

  if (!streamData) {
    logger.debug('No stream data stored, leaving cleanup.');
    return;
  }

  const liveApiClient = await getLiveApiClient();
  await liveApiClient.deleteLiveStream(streamData.liveApiId);
  await storageManager.storeData(storeKey, null);

  logger.debug('Cleanup succeeded.');
};

# LiveAPI stream support for PeerTube

Status: Proof of concept

Attach [LiveAPI](https://liveapi.com/) streams to a PeerTube live videos.

## Getting started
1. Install plugin
1. Enter your credentials in the plugin settings page.
1. Setup a [webhook](https://dashboard.liveapi.com/settings/webhooks) with the URL from the settings page
1. Create a live video in PeerTube
1. Leave the video edit page, and go back again
1. A modal is shown with the stream configuration (RTMP URL and key)
1. Start your stream

## Todo
* ~~Auto load the video when page is loaded in WAINTING_FOR_LIVE state.~~
* Show the stream config directly when live has been created.
* Make LiveAPI to add EXT-X-ENDLIST when the stream ends to make sure it quits gently.

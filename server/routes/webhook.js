const { STORE_KEYS } = require('../constants');

module.exports = ({
  getLiveApiClient,
  settingsManager,
  storageManager,
  logger,
  socket,
  videos
}) => {
  const waitForPlaylistToBeReady = (playlistUrl) => {
    const intervalMs = 100;

    return new Promise((resolve, reject) => {
      let tries = 0;

      const checkWithDelay = () => setTimeout(async () => {
        tries++

        try {
          const { ok, status, statusText } = await fetch(playlistUrl);

          if (!ok) {
            throw Error(`${status}: ${statusText}`);
          }

          logger.debug(`Reached playlist URL ${playlistUrl}`);
          resolve();
        } catch (error) {
          if ((tries * intervalMs / 1000) > (60 * 5)) {
            logger.error(`Playlist URL not reachable, timed out after ${tries} tries. ${playlistUrl}`);
            return reject();
          }

          logger.debug(`Playlist URL not reachable (${error}), will retry in ${intervalMs} ms. ${playlistUrl}`);
          checkWithDelay();
        }
      }, intervalMs);

      checkWithDelay();
    });
  };

  const connectVideoToLiveApiStream = async (liveApiStreamId) => {
    const ptVideoId = await storageManager.getData(STORE_KEYS.LIVE_API_PT_ID_MAPPING_PREFIX + liveApiStreamId);
    logger.debug('Will load video: ' + ptVideoId);

    if (!(await videos.loadByIdOrUUID(ptVideoId))) {
      logger.info(`No video with id ${ptVideoId} found, won't connect to a stream.`);
      return;
    }

    logger.debug(`Will connect LiveAPI stream ${liveApiStreamId} to a PeerTube live video.`)
    const liveApiClient = await getLiveApiClient();

    {
      const { data: liveApiVideo, ok } = await liveApiClient.getLiveStream(liveApiStreamId);
      if (!ok) return;

      logger.debug('Got liveApi stream: ' + JSON.stringify({ liveApiVideo }, null, 2));
      if (liveApiVideo.broadcasting_status === 'offline') {
        logger.warn(`Expected LiveAPI stream ${liveApiStreamId} to be online.`);
        return;
      }

      const playlistUrl = liveApiVideo.playback.hls_url;

      await waitForPlaylistToBeReady(playlistUrl);

      logger.debug('Loaded video: ' + JSON.stringify(video, null, 2));

      const streamData = await storageManager.getData(STORE_KEYS.STREAM_DATA_PREFIX + video.uuid);
      await storageManager.storeData(STORE_KEYS.STREAM_DATA_PREFIX + video.uuid, {
        ...streamData,
        playlistUrl
      })

      const video = await videos.loadByIdOrUUID(ptVideoId); // Reload video to ensure it hasn't changed

      if (!video) {
        logger.info(`No video with id ${ptVideoId} found, won't connect to a stream.`);
        return;
      }

      video.state = 1; // PUBLISHED
      video.publishedAt = new Date()
      video.save();

      socket.sendVideoLiveNewState(video);
    }
  };

  const endLive = async (liveApiStreamId) => {
    const { forgivenessTimeout } = await settingsManager.getSettings(['forgivenessTimeout']);
    const forgivenessTimeoutMs = +forgivenessTimeout * 60 * 1000;

    logger.debug(`Will wait for ${forgivenessTimeoutMs} ms before ending the live.`);

    await new Promise(resolve => {
      setTimeout(resolve, forgivenessTimeoutMs);
    });

    const liveApiClient = await getLiveApiClient();
    const { data: liveApiVideo, ok } = await liveApiClient.getLiveStream(liveApiStreamId);

    if (ok) {
      if (liveApiVideo.broadcasting_status === 'online') {
        logger.debug(`LiveAPI stream ${liveApiStreamId} is back online, won't end live.`);
        return
      }
    }

    logger.debug(`Will end live stream attached to LiveAPI stream ${liveApiStreamId}.`)
    const ptVideoId = await storageManager.getData(STORE_KEYS.LIVE_API_PT_ID_MAPPING_PREFIX + liveApiStreamId);

    const video = await videos.loadByIdOrUUID(ptVideoId);

    if (!video) {
      logger.debug(`Video ${ptVideoId} has been removed, won't update video state.`);
      return;
    }

    video.state = 5; // LIVE_ENDED
    video.save();

    socket.sendVideoLiveNewState(video);
  };

  return async (req, res) => {
    logger.info('received webhook: ' + JSON.stringify(req.body, null, 2));

    const id = req.body.payload._id;
    const eventType = req.body.event;

    switch (eventType) {
      case 'stream-connected':
        connectVideoToLiveApiStream(id);
        break;
      case 'stream-disconnected':
        endLive(id);
        break;
      default:
        logger.warn('Unhandled webhook event: ' + eventType);
    }

    res.json();
  }
}

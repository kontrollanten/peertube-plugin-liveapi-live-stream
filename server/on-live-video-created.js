const { STORE_KEYS } = require('./constants');

module.exports = async ({
  getLiveApiClient,
  logger,
  settingsManager,
  storageManager,
  video,
}) => {
  const { accessTokenId, secretKey } = await settingsManager.getSettings(['accessTokenId', 'secretKey']);

  if (!secretKey || !accessTokenId) {
    logger.info('Either secretKey or accessTokenId not provided, won\'t create a LiveAPI stream.');
    return;
  }

  const liveApiClient = await getLiveApiClient();

  const { data, ok } = await liveApiClient.createLiveStream();

  if (!ok) return;

  await storageManager.storeData(STORE_KEYS.LIVE_API_PT_ID_MAPPING_PREFIX + data._id, video.id);
  await storageManager.storeData(STORE_KEYS.STREAM_DATA_PREFIX + video.uuid, {
    liveApiId: data._id,
    rtmps: {
      url: data.ingest.server,
      streamKey: data.ingest.key
    }
  });
};


const fetch = require('cross-fetch');

module.exports = ({
  accessTokenId,
  logger,
  secretKey,
}) => {
  const callServer = (path, { body, method = 'get' } = {}) => {
    const url = `https://api.liveapi.com${path}`;

    logger.debug(`liveApiClient: ${method} ${url} ${accessTokenId}`);

    return fetch(url, {
      headers: {
        Authorization:  'Basic ' + Buffer.from(accessTokenId + ':' + secretKey).toString('base64'),
        'Content-Type': 'application/json',
      },
      method,
      body: body ? JSON.stringify(body) : undefined,
    })
      .then(async res => {
        if (!res.ok) {
          logger.error(`liveApiClient: ${method} ${url} responded with ${res.status}`);
          const { error, message } = await res.json();
          logger.error(`liveApiClient: ${method} ${url}: ` + JSON.stringify({
            body,
            error,
            message,
            status: res.status
          }, null, 2));
        }

        return {
          data: await res.json(),
          ok: res.ok
        };
      })
      .catch(err => {
        logger.error(`liveApiClient: Failed to ${method} ${url}: ` + JSON.stringify({ err }, null, 2));

        return err || {};
      });
  };

  return {
    createLiveStream: () => {
      return callServer('/live_streams', {
        method: 'post',
      });
    },
    deleteLiveStream: (id) => callServer(`/live_streams/${id}`, {
      method: 'delete'
    }),
    getLiveStream: (id) => callServer(`/live_streams/${id}`),
  };
};

